#include <stdio.h>
int main(){
int A=10, B=15;
	printf("AND operator (A&B): %d\n",A&B);
	printf("OR operator (A|B): %d\n",A|B);
	printf("NOT operator (~A): %d\n",~A);
	printf("RIGHT SHIFT operator (A<<3): %d\n",A<<3);
	printf("LEFT SHIFT operator (B>>3): %d\n",B>>3);
	return 0;
}
